import React from "react";
import ReactDOM from "react-dom/client";

/**
 * Copy the esbuild-wasm binary into the public resources folder
const App = ()=>{
return <div>meow</div>;
}
 */
import * as esbuild from "esbuild-wasm";

import { unpkgPathPlugin } from "./plugins/unpkgPathPlugin";

import { useState, useCallback, useEffect, useRef } from "react";

const App = () => {
  const [userInput, setUserInput] = useState<string>(
    `const App = () => {
        return <div>meow</div>;
      }`
  );
  const [transpiledCode, setTranspiledCode] = useState<string>("");
  const esBuildServiceRef = useRef<esbuild.Service | undefined>(undefined);

  const transpileHandler = useCallback((userCode: string) => {
    console.log(userCode);
    if (esBuildServiceRef.current) {
      esBuildServiceRef.current
        .build({
          entryPoints: ["index.js"],
          bundle: true,
          write: false,
          plugins: [unpkgPathPlugin()],
        })
        .then((bundledOutput) => {
          setTranspiledCode(bundledOutput.outputFiles[0].text);
        });
      /*
      // transpile the code
      esBuildServiceRef.current
        .transform(userCode, {
          loader: "jsx",
          target: "es2015",
        })
        .then((transpiledResult: esbuild.TransformResult) => {
          console.log("transpiled result: ", transpiledResult);
          setTranspiledCode(transpiledResult.code);
        });
        */
    }
  }, []);

  useEffect(() => {
    const startService = async () => {
      esBuildServiceRef.current = await esbuild.startService({
        worker: true,
        wasmURL: "/esbuild.wasm",
      });
    };
    startService();
  }, []);

  return (
    <div>
      <textarea
        value={userInput}
        onChange={(event) => {
          //console.log("event ", event.target?.value);
          if (event.target?.value) {
            setUserInput(event.target?.value);
          }
        }}
      />
      <div>
        <button
          onClick={() => {
            if (!esBuildServiceRef.current || !userInput) {
              // if transpile service not initialized return
              return;
            }
            transpileHandler(userInput);
          }}
        >
          Transpile Code
        </button>
      </div>
      <pre>{transpiledCode}</pre>
    </div>
  );
};

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
