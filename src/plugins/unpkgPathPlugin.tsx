import * as esbuild from "esbuild-wasm";

import axios from "axios";

export const unpkgPathPlugin = () => {
  /*
   * We have two testing packages on npm we can test this with: tiny-test-pkg, medium-test-pkg.
   * tiny-test-pkg is composed of a single index.js file. medium-test-pkg is composed of two js files.
   */
  return {
    name: "unpkg-path-plugin",
    setup(build: esbuild.PluginBuild) {
      build.onResolve({ filter: /.*/ }, async (args: esbuild.OnResolveArgs) => {
        console.log("onResole", args);
        /**
         * Relevant arguments:
         *  path: "./utils"
         *  importer: examples - "index.js", "https://www.unpkg.com/medium-test-pkg"
         *  namespace:
         *  resolveDir:
         */
        if (args.path === "index.js") {
          return { path: args.path, namespace: "a" };
        }
        /* else if (args.path === "tiny-test-pkg") {
          return {
            path: "https://www.unpkg.com/tiny-test-pkg@1.0.0/index.js",
            namespace: "a",
          };
        }*/
        // this path will redirect us to the index.js of the package
        //let resolvedPath = args.path;
        //if(args.path === 'index.js') {

        //}
        let resolvedURL: URL | undefined = undefined; //new URL(args.path);
        if (args.importer === "index.js") {
          //resolvedPath = `https://www.unpkg.com/${args.path}`;

          resolvedURL = new URL(args.path, "https://www.unpkg.com");

          //const response = await axios.get(resolvedURL.href);
          //console.log("index response: ", response);
          //resolvedURL = response.request.responseURL.replace("index.js", "");
          //return {
          //  path: new URL(args.path, "https://www.unpkg.com").href,
          //  namespace: "a",
          //};
        } else {
          //can check if importer path starts with https://
          //resolvedPath = `${args.importer}/${args.path}`;
          /**
           * unpkg redirects us to the main JS file:
           *      path = https://www.unpkg.com/nested-test-pkg => redirected: https://www.unpkg.com/nested-test-pkg@1.0.0/src/index.js
           * So further requests need to be made relative to https://www.unpkg.com/nested-test-pkg/src/
           */
          resolvedURL = new URL(args.path, args.importer + "/");
        }
        console.log("onResolve: Resolved Path - ", resolvedURL.href);
        return {
          path: resolvedURL ? resolvedURL.href : "",
          namespace: "a",
        };
      });

      build.onLoad({ filter: /.*/ }, async (args: esbuild.OnLoadArgs) => {
        console.log("onLoad", args);

        if (args.path === "index.js") {
          return {
            loader: "jsx",
            contents: `
              import message from 'medium-test-pkg';
              console.log(message);
            `,
          };
        }

        //fetch(args.path).then((data: any) => {
        //  console.log("fetched data: ", data);
        //JSON.parse(data)
        //});
        const responseData = await axios.get(args.path);
        console.log("response Data", responseData);
        let redirectURL: string | undefined = undefined;
        if (responseData?.request?.responseURL) {
          redirectURL = new URL("./", responseData.request.responseURL).href; // removes the trailing filename if any
          console.log("resolveDir ", redirectURL);
        }
        const { data } = responseData;
        console.log("onLoad: Loaded data ", data);
        return {
          loader: "jsx",
          contents: data,
          resolveDir: redirectURL ?? redirectURL,
        };
      });
    },
  };
};
